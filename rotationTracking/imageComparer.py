LENGTH = 360


def compare_graphs(graph1, graph2, threshold = 30):
    assert len(graph1) == LENGTH and len(graph2) == LENGTH

    for shift in range(0, LENGTH):
        result = True

        for count in range(0, LENGTH):
            a1 = graph1[(shift + count) % LENGTH]
            a2 = graph2[count]
            value = abs(a1 - a2)
            if not (-threshold < value < threshold):
                result = False
                break

        if result:
            return 360 - (shift * (360 / LENGTH))


