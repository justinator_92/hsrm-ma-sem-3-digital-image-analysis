import cv2
import math


def process_image(image, file_name ="pic.png"):
    # apply gauss filter to image
    #cv2.imwrite("raw.png", image)
    blur = cv2.GaussianBlur(image, (5, 5), 0)
    #cv2.imwrite("blur.png", blur)
    ret3, th3 = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    cv2.imwrite("otsu.png", th3)
    # find center point of image
    point = center_point(th3)

    if point != None:
        degrees = create_subarray(th3, point)
        cv2.circle(th3, point, 5, (0, 0, 0), -1)
        cv2.imwrite(file_name, th3)
        return (th3, degrees, point)


def center_point(image):
    height, width = image.shape[:2]
    sumX, sumY = 0, 0
    countX, countY = 0, 0
    for x in range(width):
        for y in range(height):
            if image[y, x] != 0:
                sumX, sumY = sumX + x, sumY + y
                countX, countY = countX + 1, countY + 1
    return (sumX / countX, sumY / countY)


def rot_matrix(angle, vector):
    cos_alpha = math.cos(angle * math.pi / 180.0)
    sin_alpha = math.sin(angle * math.pi / 180.0)

    return (cos_alpha * vector[0] + -sin_alpha * vector[1],
            sin_alpha * vector[0] + cos_alpha * vector[1])


def walk_from_center_point(image, vector, center):
    height, width = image.shape[:2]
    (x, y) = float(center[0]), float(center[1])

    while 0 < x < width and 0 < y < height:
        x += vector[0]
        y += vector[1]

        if image[to_int(y), to_int(x)] == 0:  # border detected..
            break

    d_x = to_int(x) - center[0]
    d_y = to_int(y) - center[1]

    distance = math.sqrt(d_x * d_x + d_y * d_y)
    return to_int(distance)


def to_int(x):
    return int((x + 0.5))


def create_subarray(image, center_point):

    degrees = []

    for e in range(0, 360):  # all 5 degrees...
        vec = rot_matrix(e, (1, 0))
        degrees.append(walk_from_center_point(image, vec, center_point))

    return degrees