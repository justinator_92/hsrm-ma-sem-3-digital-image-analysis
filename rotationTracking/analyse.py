import cv2
from matplotlib import pyplot as plt
import imageProcessing
import imageComparer
import sys


def process_images(reference_image_path, comparison_image_path):
    reference_image = cv2.imread(reference_image_path, cv2.IMREAD_ANYCOLOR)
    comparison_image = cv2.imread(comparison_image_path,cv2.IMREAD_ANYCOLOR)

    (processedImage, degrees, center) = imageProcessing.process_image(reference_image, "reference.png")
    (processedImage2, degrees2, center2) = imageProcessing.process_image(comparison_image, "comparison.png")

    print "translation: (" + str(center2[0] - center[0]) +"," + str(center2[1] - center[1]) + ")"

    # print sum(degrees) / len(degrees), sum(degrees2) / len(degrees2)

    angle = imageComparer.compare_graphs(degrees, degrees2)
    print "rotation angle: " + str(angle)

    plt.title('Reference Graph')
    plt.plot([x for x in range(0, 360)], degrees, 'g')
    plt.axis([0, 360, 0, 300])
    plt.figure()
    plt.title('Comparison Graph')
    plt.plot([x for x in range(0, 360)], degrees2, 'g')
    plt.axis([0, 360, 0, 300])

    plt.figure()
    plt.title('Reference Image')
    plt.imshow(processedImage, 'gray')
    plt.figure()
    plt.title('Comparison Image')
    plt.imshow(processedImage2, 'gray')
    plt.show()



if __name__ == '__main__':
    if len(sys.argv) < 3:
        print "please insert path to files... \npython analyse.py <ref_img> <comp_img>"
        exit(-1)
    process_images(sys.argv[1], sys.argv[2])
    #im = cv2.imread(sys.argv[1], cv2.IMREAD_ANYCOLOR)
    #ret3, th3 = cv2.threshold(im, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    #cv2.imwrite("test.bmp", th3)