﻿using ISIP_FrameworkGUI.MVVMBase;
using OxyPlot;
using System.Collections.Generic;
using Emgu.CV;
using Emgu.CV.Structure;

namespace ISIP_FrameworkGUI.ViewModels
{
    public class HistogrammViewModel : ViewModelBase
    {
        private Image<Gray, byte> _originalGrayscaleImage;
        public string Title { get; private set; }
        public IList<DataPoint> Points { get; private set; }

        public HistogrammViewModel(Image<Gray, byte> originalGrayscaleImage)
        {
            Title = "Grayscale Histogramm";
            _originalGrayscaleImage = originalGrayscaleImage;
            Points = GenerateHistrogramm();
        }

        private List<DataPoint> GenerateHistrogramm()
        {
            var ret = new List<DataPoint>();
            var arr = new int[256];

            for (var i = 0; i < 256; i += 1)
                arr[i] = 0;

            foreach(var b in _originalGrayscaleImage.Bytes)
            {
                arr[b] += 1;
            }

            for(var i = 0; i < 256; i += 1)
            {
                ret.Add(new DataPoint(i, arr[i]));
            }
            return ret;
        }

    }
}
