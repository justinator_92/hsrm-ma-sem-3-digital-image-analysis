﻿using Emgu.CV;
using Emgu.CV.Structure;
using ISIP_FrameworkGUI.MVVMBase;
using System.Windows.Input;
using ISIP_UserControlLibrary;
using ISIP_FrameworkGUI.Hough;

namespace ISIP_FrameworkGUI.ViewModels.Controls
{
    public class HoughLinesWindowViewModel : ViewModelBase
    {
        public Image<Gray, byte> OriginalImage { get; }

        private int _sobelThreshold = 120;
        public int SobelThreshold
        {
            get { return _sobelThreshold; }
            set
            {
                _sobelThreshold = value;
                OnPropertyChanged("SobelThreshold");
            }
        }

        private int _houghFactor = 50;
        public int HoughFactor
        {
            get { return _houghFactor; }
            set
            {
                _houghFactor = value;
                OnPropertyChanged("HoughFactor");
            }
        }

        private ImageProcessingControl _mainControl;
        private HoughLines _houghTransform;

        public HoughLinesWindowViewModel(ImageProcessingControl mainControl)
        {
            _mainControl = mainControl;
            OriginalImage = mainControl.OriginalGrayscaleImage;
            _houghTransform = new HoughLines(OriginalImage);
        }

        private ICommand _sobelFilterCommand;
        public ICommand SobelFilterCommand
        {
            get
            {
                if (_sobelFilterCommand == null)
                {
                    _sobelFilterCommand = new RelayCommand(() =>
                    {
                        _houghTransform.ApplySobelFilter(SobelThreshold);
                        _mainControl.ProcessedGrayscaleImage = _houghTransform.SobelFilteredImage;
                    }, () => true);
                }
                return _sobelFilterCommand;
            }
        }

        private ICommand _houghRoomCommand;
        public ICommand HoughRoomCommand
        {
            get
            {
                if (_houghRoomCommand == null)
                {
                    _houghRoomCommand = new RelayCommand(() =>
                    {
                        _houghTransform.CalculateHoughRoom();
                        _mainControl.ProcessedGrayscaleImage = _houghTransform.HoughRoomImage;
                    }, () => true);
                }
                return _houghRoomCommand;
            }
        }

        private ICommand _houghLinesCommand;
        public ICommand HoughLinesCommand
        {
            get
            {
                if (_houghLinesCommand == null)
                {
                    _houghLinesCommand = new RelayCommand(() =>
                    {
                        _houghTransform.CalculateHoughLines(HoughFactor / 100.0);
                        _mainControl.ProcessedColorImage = _houghTransform.HoughImage;
                    }, () => true);
                }
                return _houghLinesCommand;
            }
        }
    }
}
