﻿using ISIP_FrameworkGUI.MVVMBase;
using System.Windows.Input;
using ISIP_UserControlLibrary;
using ISIP_FrameworkGUI.Hough;
using Emgu.CV;
using Emgu.CV.Structure;

namespace ISIP_FrameworkGUI.ViewModels.Controls
{
    public class HoughCircleKnownRadiusViewModel : ViewModelBase
    {
        private ImageProcessingControl mainControl;
        private HoughCirclesKnownRadius _houghCircles;

        private int _radius;
        public int Radius
        {
            get { return _radius; }
            set
            {
                _radius = value;
                OnPropertyChanged("Radius");
            }
        }

        private int _sobelThreshold;
        public int SobelThreshold
        {
            get { return _sobelThreshold; }
            set
            {
                _sobelThreshold = value;
                OnPropertyChanged("SobelThreshold");
            }
        }

        private int _houghThreshold;
        public int HoughThreshold
        {
            get { return _houghThreshold; }
            set
            {
                _houghThreshold = value;
                OnPropertyChanged("HoughThreshold");
            }
        }

        public HoughCircleKnownRadiusViewModel(ImageProcessingControl mainControl)
        {
            this.mainControl = mainControl;
            _houghCircles = new HoughCirclesKnownRadius(mainControl.OriginalGrayscaleImage);
        }

        private ICommand _showHoughMatrixCommand;
        public ICommand ShowHoughMatrixCommand
        {
            get
            {
                if (_showHoughMatrixCommand == null)
                {
                    _showHoughMatrixCommand = new RelayCommand(() =>
                    {
                        _houghCircles.ApplyHoughTransformation(_radius);
                        mainControl.ProcessedGrayscaleImage = _houghCircles.HoughRoomImage;
                    }, () => true);
                }
                return _showHoughMatrixCommand;
            }
        }

        private ICommand _applySobelCommand;
        public ICommand ApplySobelCommand
        {
            get
            {
                if (_applySobelCommand == null)
                {
                    _applySobelCommand = new RelayCommand(() =>
                    {
                        _houghCircles.ApplySobelFilter(_sobelThreshold);
                        mainControl.ProcessedGrayscaleImage = _houghCircles.SobelFilteredImage;
                    }, () => true);
                }
                return _applySobelCommand;
            }
        }

        private ICommand _showHoughImageCommand;
        public ICommand ShowHoughImageCommand
        {
            get
            {
                if (_showHoughImageCommand == null)
                {
                    _showHoughImageCommand = new RelayCommand(() => {
                        _houghCircles.CalculateHoughImage(HoughThreshold / 100.0, Radius);
                        mainControl.ProcessedColorImage = _houghCircles.HoughImage;
                    }, () => true);
                }
                return _showHoughImageCommand;
            }
        }


    }
}
