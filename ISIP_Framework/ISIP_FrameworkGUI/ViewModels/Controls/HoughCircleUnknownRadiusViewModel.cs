﻿using ISIP_FrameworkGUI.MVVMBase;
using ISIP_UserControlLibrary;
using ISIP_FrameworkGUI.Hough;
using System.Windows.Input;

namespace ISIP_FrameworkGUI.ViewModels.Controls
{
    public class HoughCircleUnknownRadiusViewModel : ViewModelBase
    {
        private ImageProcessingControl _mainControl;
        private HoughCirclesUnkownRadius _houghCircles;

        private int _radius;
        public int Radius
        {
            get { return _radius; }
            set
            {
                _radius = value;
                OnPropertyChanged("Radius");
            }
        }

        private int _sobelThreshold;
        public int SobelThreshold
        {
            get { return _sobelThreshold; }
            set
            {
                _sobelThreshold = value;
                OnPropertyChanged("SobelThreshold");
            }
        }

        private int _houghThreshold = 50;
        public int HoughThreshold
        {
            get { return _houghThreshold; }
            set
            {
                _houghThreshold = value;
                OnPropertyChanged("HoughThreshold");
            }
        }

        public HoughCircleUnknownRadiusViewModel(ImageProcessingControl mainControl)
        {
            _mainControl = mainControl;
            _houghCircles = new HoughCirclesUnkownRadius(_mainControl.OriginalGrayscaleImage);
        }

        private ICommand _applySobelCommand;
        public ICommand ApplySobelCommand
        {
            get
            {
                if (_applySobelCommand == null)
                {
                    _applySobelCommand = new RelayCommand(() =>
                    {
                        _houghCircles.ApplySobelFilter(_sobelThreshold);
                        _mainControl.ProcessedGrayscaleImage = _houghCircles.SobelFilteredImage;
                    }, () => true);
                }
                return _applySobelCommand;
            }
        }

        private ICommand _showHoughMatrixCommand;
        public ICommand ShowHoughMatrixCommand
        {
            get
            {
                if (_showHoughMatrixCommand == null)
                {
                    _showHoughMatrixCommand = new RelayCommand(() =>
                    {
                        _houghCircles.CalculateNormalizedHoughRoom(Radius);
                        _mainControl.ProcessedGrayscaleImage = _houghCircles.HoughRoomImage;
                    }, () => true);
                }
                return _showHoughMatrixCommand;
            }
        }

        private ICommand _showHoughImageCommand;
        public ICommand ShowHoughImageCommand
        {
            get
            {
                if (_showHoughImageCommand == null)
                {
                    _showHoughImageCommand = new RelayCommand(() => {
                        _houghCircles.RateHoughRoom(HoughThreshold / 100.0);
                        _mainControl.ProcessedColorImage = _houghCircles.HoughImage;
                    }, () => true);
                }
                return _showHoughImageCommand;
            }
        }

    }
}
