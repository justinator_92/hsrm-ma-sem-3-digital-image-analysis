﻿using ISIP_FrameworkGUI.MVVMBase;
using System;

namespace ISIP_FrameworkGUI.ViewModels.Controls
{
    public class ThresholdControlViewModel : ViewModelBase
    {
        private string _adjustmentTitle;
        public string AdjustmentTitle
        {
            get { return _adjustmentTitle; }
            set
            {
                if(!string.IsNullOrWhiteSpace(value))
                {
                    _adjustmentTitle = value;
                    OnPropertyChanged("AdjustmentTitle");
                }
            }
        }

        private int _maxThreshold;
        public int MaxThreshold
        {
            get { return _maxThreshold; }
            set
            {
                if(value != _maxThreshold)
                {
                    _maxThreshold = value;
                    OnPropertyChanged("MaxThreshold");
                }
            }
        }

        private int _currentThreshold;
        public int CurrentThreshold
        {
            get { return _currentThreshold; }
            set
            {
                if(value != _currentThreshold)
                {
                    _currentThreshold = value / Scale;
                    OnPropertyChanged("CurrentThreshold");
                    CallbackOnThresholdChanged.Invoke(_currentThreshold);
                }
            }
        }

        public int Scale { get; set; }

        public Action<int> CallbackOnThresholdChanged { get; set; }

        public ThresholdControlViewModel(string title, int maxThreshold, Action<int> callback) : this(title, maxThreshold, callback, 1)
        {
        }

        public ThresholdControlViewModel(string title, int maxThreshold, Action<int> callback, int scale)
        {
            AdjustmentTitle = title;
            MaxThreshold = maxThreshold;
            CallbackOnThresholdChanged = callback;
            Scale = scale;
        }

    }
}
