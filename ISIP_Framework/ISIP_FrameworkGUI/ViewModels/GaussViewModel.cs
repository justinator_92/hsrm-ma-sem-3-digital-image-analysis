﻿using ISIP_FrameworkGUI.MVVMBase;
using OxyPlot;
using System.Collections.Generic;
using System.Windows.Input;

namespace ISIP_FrameworkGUI.ViewModels
{
    public class GaussViewModel : ViewModelBase
    {
        public IList<DataPoint> Points { get; private set; }
        public string Title { get; private set; }

        private float _kValue;
        public float KValue
        {
            get { return _kValue; }
            set
            {
                if (value > 0)
                {
                    _kValue = value;
                    OnPropertyChanged("KValue");
                }
            }
        }

        private float _sigmaValue;
        public float SigmaValue
        {
            get { return _sigmaValue; }
            set
            {
                if (value > 0)
                {
                    _sigmaValue = value;
                    OnPropertyChanged("SigmaValue");
                }
            }
        }

        public GaussViewModel()
        {
            Title = "Gauss Graph";
        }

        private int ToInt(double value)
        {
            return (int)(value + 0.5);
        }

        private double Gauss(double x)
        {
            return 100.0 * System.Math.Exp(-(((x - KValue) * (x - KValue))) / (2 * (SigmaValue * SigmaValue)));
        }

        private List<DataPoint> GeneratePoints()
        {
            var ret = new List<DataPoint>();
            for (var x = 0; x < 200; x += 1)
            {
                ret.Add(new DataPoint(x, ToInt(Gauss(x))));
            }
            return ret;
        }

        private ICommand _redrawCommand;
        public ICommand RedrawCommand
        {
            get
            {
                if (_redrawCommand == null)
                    _redrawCommand = new RelayCommand(() =>
                    {
                        Points = GeneratePoints();
                        OnPropertyChanged("Points");
                    }, () => true);
                return _redrawCommand;
            }
        }

    }
}
