﻿using Emgu.CV;
using Emgu.CV.Structure;
using ISIP_FrameworkGUI.Filter.HighPassFilter;
using ISIP_FrameworkGUI.MVVMBase;
using ISIP_UserControlLibrary;
using System.Windows.Input;

namespace ISIP_FrameworkGUI.ViewModels
{
    public class HoughLinesViewModel : ViewModelBase
    {
        public SobelFilter SobelFilter { get; set; }
        public Image<Gray, byte> OriginalGrayscaleImage { get; set; }
        public ImageProcessingControl MainControl { get; set; }
        private int _thresholdFilter;
        public int ThresholdFilter
        {
            get { return _thresholdFilter; }
            set
            {
                _thresholdFilter = value;
                OnPropertyChanged("ThresholdFilter");
            }
        }

        public HoughLinesViewModel(ImageProcessingControl mainControl)
        {
            MainControl = mainControl;
            OriginalGrayscaleImage = mainControl.OriginalGrayscaleImage;
            ThresholdFilter = 124;
            SobelFilter = new SobelFilter(OriginalGrayscaleImage, ThresholdFilter, "Binary");
        }

        private ICommand _sobelCommand;
        public ICommand SobelCommand
        {
            get
            {
                if (_sobelCommand == null)
                    _sobelCommand = new RelayCommand(() =>
                    {
                        SobelFilter.Threshold = ThresholdFilter;
                        MainControl.ProcessedGrayscaleImage = SobelFilter.ApplyFilter();
                    }, () => true);
                return _sobelCommand;
            }
        }



    }
}
