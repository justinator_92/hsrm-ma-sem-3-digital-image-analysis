﻿using Emgu.CV;
using Emgu.CV.Structure;

namespace ISIP_FrameworkGUI.ImageTools
{
    public class ImageManipulation
    {
        public Image<Gray, byte> MirrorImage(Image<Gray, byte> image)
        {
            var imgWidth = image.Width;
            var imgHeight = image.Height;

            var src = image.Bytes;
            var dst = new byte[imgHeight * imgWidth];

            for (int y = 0; y < imgHeight; y++)
            {
                for (int x = 0; x < imgWidth; x++)
                {
                    dst[imgWidth * y + x] = src[imgWidth * y + (imgWidth - 1) - x];
                }
            }

            Image<Gray, byte> returnImg = new Image<Gray, byte>(imgWidth, imgHeight);
            returnImg.Bytes = dst;
            return returnImg;
        }
        
        public Image<Gray, byte> BinarizeImage(Image<Gray, byte> image, int threshold)
        {
            var imgWidth = image.Width;
            var imgHeight = image.Height;

            byte pixel;

            var src = image.Bytes;
            var dst = new byte[imgHeight * imgWidth];

            for (int y = 0; y < imgHeight; y++)
            {
                for (int x = 0; x < imgWidth; x++)
                {
                    pixel = src[imgWidth * y + x];
                    dst[imgWidth * y + x] = pixel > threshold ? (byte)255 : (byte)0;
                }
            }

            Image<Gray, byte> returnImg = new Image<Gray, byte>(imgWidth, imgHeight);
            returnImg.Bytes = dst;
            return returnImg;
        }

    }
}
