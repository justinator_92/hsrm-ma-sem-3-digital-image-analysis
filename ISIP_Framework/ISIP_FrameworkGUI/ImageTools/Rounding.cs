﻿namespace ISIP_FrameworkGUI.ImageTools
{
    public static class Rounding
    {
        public static int ToInt(double value)
        {
            return (int)(value + 0.5);
        }
    }
}
