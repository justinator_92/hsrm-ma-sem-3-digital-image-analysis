﻿using Emgu.CV;
using Emgu.CV.Structure;
using ISIP_FrameworkGUI.ImageTools;
using System.Linq;

namespace ISIP_FrameworkGUI.Filter.HighPassFilter
{
    public class AdaptiveSobelFilter : FilterBase
    {
        public double Factor { get; set; }
        public double AdaptiveThreshold { get; set; }
        public double MinimumThreshold { get; set; }

        public AdaptiveSobelFilter(Image<Gray, byte> originalImage, double factor) : base(originalImage, 11, "high")
        {
            Factor = factor;
            MinimumThreshold = 20;
            ImageBytes = new SobelFilter(originalImage).ApplyFilterGetAsDouble().Select(x => Rounding.ToInt(x)).ToArray();
        }

        public override Image<Gray, byte> ApplyFilter()
        {
            return base.ApplyFilter();
        }

        protected override void CreateSubMatrix()
        {
        }

        protected override double ApplyFilterForPixel(double[] src)
        {
            var min = src.Min();
            var max = src.Max();
            AdaptiveThreshold = Factor * max + (1 - Factor) * min;

            var pixel = src[SubMatrixSize / 2];
            var threshold = AdaptiveThreshold < MinimumThreshold ? MinimumThreshold : AdaptiveThreshold;
            return pixel < threshold ? 0 : 255;
        }
    }
}
