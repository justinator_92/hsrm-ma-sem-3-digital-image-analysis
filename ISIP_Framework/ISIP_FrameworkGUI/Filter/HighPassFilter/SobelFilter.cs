﻿using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;

namespace ISIP_FrameworkGUI.Filter.HighPassFilter
{
    public class SobelFilter : FilterBase
    {
        public double[] Hx { get; private set; }
        public double[] Hy { get; private set; }
        public int Threshold { get; set; }
        public string Reduce { get; set; }
        private double _gradX;
        private double _gradY;
        private Dictionary<string, Func<double, double, int, double>> _reduceFunc = new Dictionary<string, Func<double, double, int, double>>
        {
            {"grad", (double gradX, double gradY, int T) => { return Math.Sqrt(gradX * gradX + gradY * gradY); } },
            {"directions", (double gradX, double gradY, int T) => { return Math.Atan2(gradX, gradY); } }, // in radians
            {"binary", (double gradX, double gradY, int T) => { return Math.Sqrt(gradX * gradX + gradY * gradY) <= T ? 0 : 255; } }
        };

        public SobelFilter(Image<Gray, byte> originalImage) : this(originalImage, 0, "grad")
        {
        }

        public SobelFilter(Image<Gray, byte> originalImage, int threshold, string reduce) : base(originalImage, 3, "high")
        {
            Threshold = threshold;
            Reduce = reduce;
        }

        protected override double ApplyFilterForPixel(double[] src)
        {
            Func<double, double, int, double> reduceFunc;
            _reduceFunc.TryGetValue(Reduce.ToLower(), out reduceFunc);

            _gradX = Hx[2] * src[2] + Hx[0] * src[0] +
                     Hx[5] * src[5] + Hx[3] * src[3] +
                     Hx[8] * src[8] + Hx[6] * src[6];

            _gradY = Hy[8] * src[8] + Hy[2] * src[2] +
                     Hy[7] * src[7] + Hy[1] * src[1] +
                     Hy[6] * src[6] + Hy[0] * src[0];

            return reduceFunc.Invoke(_gradX, _gradY, Threshold);
        }

        protected override void CreateSubMatrix()
        {
            Hy = new double[] {-1, 0, 1,
                               -2, 0, 2,
                               -1, 0, 1};

            Hx = new double[] {-1, -2, -1,
                                0,  0,  0,
                                1,  2,  1 };
        }

    }
}
