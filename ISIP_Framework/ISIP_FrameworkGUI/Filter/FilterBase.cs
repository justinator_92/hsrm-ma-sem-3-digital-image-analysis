﻿using Emgu.CV;
using Emgu.CV.Structure;
using ISIP_FrameworkGUI.ImageTools;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ISIP_FrameworkGUI.Filter
{
    public abstract class FilterBase
    {
        public int ImageWidth { get; private set; }
        public int ImageHeight { get; private set; }
        public int SubMatrixSize { get; private set; }
        public double[] SubMatrix { get; set; }
        public int[] ImageBytes { get; set; }
        public string FilterType { get; private set; }
        private double[] _currentSubMatrix;
        private int _halfWidth;
        private Dictionary<string, Func<int, byte>> _borderFunc = new Dictionary<string, Func<int, byte>>
        {
            { "high", (int d) => { return 0; } },
            { "low", (int d) => { return (byte)d; } }
        };

        public FilterBase(Image<Gray, byte> originalImage, int subMatrixSize, string filterType)
        {
            ImageWidth = originalImage.Width;
            ImageHeight = originalImage.Height;
            SubMatrixSize = subMatrixSize;
            _currentSubMatrix = new double[SubMatrixSize * SubMatrixSize];
            _halfWidth = SubMatrixSize / 2;
            ImageBytes = originalImage.Bytes.Select(x => Convert.ToInt32(x)).ToArray();
            FilterType = filterType;
            CreateSubMatrix();
        }

        public virtual Image<Gray, byte> ApplyFilter()
        {
            var ret = new Image<Gray, byte>(ImageWidth, ImageHeight);
            var bytes = new byte[ImageHeight * ImageWidth];
            var pixels = ApplyFilterGetAsDouble();

            for (int i = 0; i < ImageHeight * ImageWidth; i += 1)
            {
                bytes[i] = (byte)Rounding.ToInt(pixels[i]);
            }

            ret.Bytes = bytes;
            return ret;
        }

        public double[] ApplyFilterGetAsDouble()
        {
            var bytes = new double[ImageHeight * ImageWidth];
            Func<int, byte> borderFunc;
            _borderFunc.TryGetValue(FilterType.ToLower(), out borderFunc);

            for (var y = 0; y < ImageHeight; y += 1)
            {
                for (var x = 0; x < ImageWidth; x += 1)
                {
                    // check if (x, y) is valid
                    if (x >= SubMatrixSize / 2 && x < (ImageWidth - SubMatrixSize / 2) && 
                        y >= SubMatrixSize / 2 && y < (ImageHeight - SubMatrixSize / 2))
                    {
                        GetSubMatrixFromPixel(x, y);
                        bytes[y * ImageWidth + x] = ApplyFilterForPixel(_currentSubMatrix);
                    }
                    else // take pixel from source image...
                    {
                        bytes[y * ImageWidth + x] = borderFunc.Invoke(ImageBytes[y * ImageWidth + x]);
                    }
                }
            }
            return bytes;
        }

        private void GetSubMatrixFromPixel(int x, int y)
        {
            int xValue, yValue, idx;
            for (var height = 0; height < SubMatrixSize; height += 1)
            {
                for (var width = 0; width < SubMatrixSize; width += 1)
                {
                    xValue = x + (width - _halfWidth);
                    yValue = y + (height - _halfWidth);
                    idx = GetOneDimensionalIndex(xValue, yValue, ImageWidth);
                    _currentSubMatrix[height * SubMatrixSize + width] = ImageBytes[idx];
                }
            }
        }

        private int GetOneDimensionalIndex(int x, int y, int width)
        {
            return y * width + x;
        }

        protected abstract void CreateSubMatrix();
        protected abstract double ApplyFilterForPixel(double[] src);
    }
}
