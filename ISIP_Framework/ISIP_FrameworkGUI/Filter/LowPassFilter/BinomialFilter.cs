﻿using Emgu.CV;
using Emgu.CV.Structure;
using System.Collections.Generic;
using System.Linq;

namespace ISIP_FrameworkGUI.Filter.LowPassFilter
{
    public class BinomialFilter : FilterBase
    {
        private Image<Gray, byte> _originalGrayscaleImage { get; set; }

        public BinomialFilter(Image<Gray, byte> originalGrayscaleImage, int size) : base(originalGrayscaleImage, size, "low")
        {
            _originalGrayscaleImage = originalGrayscaleImage;
        }

        protected override double ApplyFilterForPixel(double[] src)
        {
            var ret = 0.0;
            for (var i = 0; i < (SubMatrixSize * SubMatrixSize); i += 1)
            {
                ret += src[i] * SubMatrix[i];
            }
            return ret;
        }

        private double[] GetCoefficientsFromPascalTriangle()
        {
            var results = new List<double>();
            var value = 1.0;
            var n = SubMatrixSize - 1;
            results.Add(value);
            for (var k = 1; k <= n; k++)
            {
                value = (value * (n + 1 - k)) / k;
                results.Add(value);
            }
            return results.ToArray();
        }

        protected override void CreateSubMatrix()
        {
            var matrix = new double[SubMatrixSize * SubMatrixSize];
            var pascalTriangle = GetCoefficientsFromPascalTriangle();
            for (var x = 0; x < SubMatrixSize; x += 1)
            {
                for (var y = 0; y < SubMatrixSize; y += 1)
                {
                    matrix[y * SubMatrixSize + x] = pascalTriangle[x] * pascalTriangle[y];
                }
            }
            var value = matrix.Sum();
            SubMatrix = matrix.Select(e => (1 / value) * e).ToArray();
        }

    }
}
