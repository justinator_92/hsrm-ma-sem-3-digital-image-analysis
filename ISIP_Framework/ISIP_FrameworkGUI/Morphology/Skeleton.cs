﻿using Emgu.CV;
using Emgu.CV.Structure;
using System.Collections.Generic;

namespace ISIP_FrameworkGUI.Morphology
{
    public class Skeleton
    {
        public List<int[]> MaskElements = new List<int[]>
        {
            new int[] { -1, -1, 0, -1, 1, 1, 0, 1, 0 },
            new int[] { 1, 0, -1, 1, 1, -1, 1, 0, -1 },
            new int[] { 0, 1, 0, -1, 1, 1, -1, -1, 0 },
            new int[] { 1, 1, 1, 0, 1, 0, -1, -1, -1 },
            new int[] { 0, 1, 0, 1, 1, -1, 0, -1, -1 },
            new int[] { -1, 0, 1, -1, 1, 1, -1, 0, 1 },
            new int[] { 0, -1, -1, 1, 1, -1, 0, 1, 0 },
            new int[] { -1, -1, -1, 0, 1, 0, 1, 1, 1 }
        };
        public int ImageWidth { get; private set; }
        public int ImageHeight { get; private set; }
        private List<Point> _deletionPoints = new List<Point>();
        private int[] _subMatrix;
        private byte[] _imageBytes;

        public Skeleton(Image<Gray, byte> originalImage)
        {
            ImageWidth = originalImage.Width;
            ImageHeight = originalImage.Height;
            _imageBytes = originalImage.Bytes;
            _subMatrix = new int[9];
        }

        public Image<Gray, byte> CalculateSkeletonImage()
        {
            var retImage = new Image<Gray, byte>(ImageWidth, ImageHeight);
            var lastChange = false;
            do
            {
                lastChange = CalculateSkeleton();
            } while (lastChange);

            retImage.Bytes = _imageBytes;
            return retImage;
        }

        private bool CalculateSkeleton()
        {
            var ret = false;
            for (int i = 0; i < MaskElements.Count; i += 1)
            {
                if (ApplyMask(MaskElements[i]))
                {
                    ret = true;
                }
            }
            return ret;
        }

        private bool ApplyMask(int[] mask)
        {
            var change = false;
            _deletionPoints.Clear();
            for (var y = 1; y < ImageHeight - 1; y += 1)
            {
                for (var x = 1; x < ImageWidth - 1; x += 1)
                {
                    GetSubMatrix(x, y);
                    if (ApplySkeletonMaskForPixel(mask))
                    {
                        _deletionPoints.Add(new Point { X = x, Y = y });
                        change = true;
                    }
                }
            }

            if (!change)
            {
                return false;
            }

            foreach (var point in _deletionPoints)
            {
                _imageBytes[point.Y * ImageWidth + point.X] = 0;
            }
            return true;
        }

        private void GetSubMatrix(int x, int y)
        {
            int xValue, yValue;
            for (var height = 0; height < 3; height += 1)
            {
                for (var width = 0; width < 3; width += 1)
                {
                    xValue = x + (width - 1);
                    yValue = y + (height - 1);
                    _subMatrix[height * 3 + width] = _imageBytes[yValue * ImageWidth + xValue];
                }
            }
        }

        private bool ApplySkeletonMaskForPixel(int[] mask)
        {
            for (var i = 0; i < 9; i += 1)
            {
                if (mask[i] == 0)
                {
                    continue;
                }
                if (mask[i] == 1 && _subMatrix[i] == 0)
                {
                    return false;
                }
                if (mask[i] == -1 && _subMatrix[i] == 255)
                {
                    return false;
                }
            }
            return true;
        }

        public class Point
        {
            public int X { get; set; }
            public int Y { get; set; }
        }
    }
}
