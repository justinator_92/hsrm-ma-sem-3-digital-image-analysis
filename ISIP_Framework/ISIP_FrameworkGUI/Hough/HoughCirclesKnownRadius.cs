﻿using Emgu.CV;
using Emgu.CV.Structure;
using ISIP_FrameworkGUI.Filter.HighPassFilter;
using ISIP_FrameworkGUI.ImageTools;
using ISIP_FrameworkGUI.Morphology;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ISIP_FrameworkGUI.Hough
{
    public class HoughCirclesKnownRadius
    {
        public Image<Gray, byte> OriginalImage { get; private set; }
        public Image<Gray, byte> SobelFilteredImage { get; private set; }
        public Image<Gray, byte> HoughRoomImage { get; private set; }
        public Image<Bgr, byte> HoughImage { get; private set; }
        public int ImageWidth { get; private set; }
        public int ImageHeight { get; private set; }
        private int[] _houghRoom;
        private double[] _scaledHoughRoom;
        private IList<Cluster> _clusters = new List<Cluster>();

        public HoughCirclesKnownRadius(Image<Gray, byte> originalImage)
        {
            OriginalImage = originalImage;
            ImageWidth = originalImage.Width;
            ImageHeight = originalImage.Height;
            HoughRoomImage = new Image<Gray, byte>(ImageWidth, ImageHeight);
        }

        public void ApplySobelFilter(int threshold)
        {
            SobelFilteredImage = new SobelFilter(OriginalImage, threshold, "Binary").ApplyFilter();
            SobelFilteredImage = new Skeleton(SobelFilteredImage).CalculateSkeletonImage();
        }

        public void ApplyHoughTransformation(int radius)
        {
            _houghRoom = new int[ImageWidth * ImageHeight];
            var bytes = SobelFilteredImage.Bytes;
            int xPos, yPos;
            for (var x = 0; x < ImageWidth; x += 1)
            {
                for (var y = 0; y < ImageHeight; y += 1)
                {
                    if (bytes[y * ImageWidth + x] > 0)  // for each white pixel...
                    {
                        for (var res = 0; res < 360; res += 2)
                        {
                            xPos = (int)(x + radius * Math.Cos(res * Math.PI / 180.0));
                            yPos = (int)(y + radius * Math.Sin(res * Math.PI / 180.0));
                            if (xPos > 0 && xPos < ImageWidth && yPos > 0 && yPos < ImageHeight)
                            {
                                _houghRoom[yPos * ImageWidth + xPos] += 1;
                            }
                        }
                    }
                }
            }
            ScaleHoughRoomImage();
        }

        private void ScaleHoughRoomImage()
        {
            var bytes = new byte[ImageWidth * ImageHeight];
            _scaledHoughRoom = new double[ImageWidth * ImageHeight];
            var min = _houghRoom.Min();
            var max = _houghRoom.Max();
            var fraction = Convert.ToDouble(max) - min;
            for (var idx = 0; idx < (ImageWidth * ImageHeight); idx += 1)
            {
                _scaledHoughRoom[idx] = (_houghRoom[idx] - min) / fraction;
            }
            for (var idx = 0; idx < (ImageWidth * ImageHeight); idx += 1)
            {
                bytes[idx] = (byte)(_scaledHoughRoom[idx] * 255);
            }
            HoughRoomImage.Bytes = bytes;
        }

        public void CalculateHoughImage(double houghThres, int radius)
        {
            _clusters = new List<Cluster>();
            for (var x = 0; x < ImageWidth; x += 1)
            {
                for (var y = 0; y < ImageHeight; y += 1)
                {
                    if (_scaledHoughRoom[y * ImageWidth + x] >= houghThres)
                    {
                        AddToCluster(radius, x, y);
                    }
                }
            }
            Console.WriteLine("Clusters found: " + _clusters.Count);
            foreach (var cluster in _clusters)
            {
                Console.WriteLine(cluster);
            }
            CreateHoughImage(radius);
        }

        private void AddToCluster(int radius, int x, int y)
        {
            if (_clusters.Count == 0)
            {
                _clusters.Add(new Cluster(radius, x, y));
                return;
            }

            var add = false;
            foreach (var cluster in _clusters)
            {
                var newX = cluster.X - x;
                var newY = cluster.Y - y;
                if (Math.Sqrt(newX * newX + newY * newY) <= radius)
                {
                    cluster.AddPoint(x, y);
                    add = true;
                }
            }
            if (!add)
            {
                _clusters.Add(new Cluster(radius, x, y));
            }
        }

        private void CreateHoughImage(int radius)
        {
            HoughImage = OriginalImage.Convert<Bgr, byte>();
            var bytes = HoughImage.Bytes; // for b,g,r channels
            foreach (var cluster in _clusters)
            {
                int xPos, yPos, idx;
                for (var degree = 0; degree < 360; degree += 1)
                {
                    xPos = Rounding.ToInt(cluster.X + radius * Math.Cos(degree * Math.PI / 180.0));
                    yPos = Rounding.ToInt(cluster.Y + radius * Math.Sin(degree * Math.PI / 180.0));
                    idx = (yPos * ImageWidth + xPos) * 3;
                    if (idx >= 0 && idx < (ImageWidth * ImageHeight * 3))
                    {
                        bytes[idx] = 0;
                        bytes[idx + 1] = 0;
                        bytes[idx + 2] = 255;
                    }
                }
            }
            HoughImage.Bytes = bytes;
        }

        public class Cluster
        {
            public double X { get; set; }
            public double Y { get; set; }
            public int Count { get; set; }
            public int Radius { get; set; }

            public Cluster(int radius, double x, double y)
            {
                X = x;
                Y = y;
                Count = 1;
                Radius = radius;
            }

            public void AddPoint(int x, int y)
            {
                X = (X * Count + x) / (Count + 1.0);
                Y = (Y * Count + y) / (Count + 1.0);
                Count += 1;
            }

            public override string ToString()
            {
                return $"Cluster: [X={X}, Y={Y}]";
            }
        }

    }
}
