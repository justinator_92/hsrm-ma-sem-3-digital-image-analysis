﻿using Emgu.CV;
using Emgu.CV.Structure;
using ISIP_FrameworkGUI.Filter.HighPassFilter;
using ISIP_FrameworkGUI.ImageTools;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ISIP_FrameworkGUI.Hough
{
    public class HoughCirclesUnkownRadius
    {
        public Image<Gray, byte> OriginalImage { get; private set; }
        public Image<Gray, byte> SobelFilteredImage { get; private set; }
        public Image<Gray, byte> HoughRoomImage { get; private set; }
        public Image<Bgr, byte> HoughImage { get; private set; }
        public int ImageWidth { get; private set; }
        public int ImageHeight { get; private set; }
        public int MinRadius { get; private set; }
        public int MaxRadius { get; private set; }
        private int[,,] _houghRoom;

        public HoughCirclesUnkownRadius(Image<Gray, byte> originalImage)
        {
            OriginalImage = originalImage;
            ImageWidth = OriginalImage.Width;
            ImageHeight = OriginalImage.Height;
            HoughRoomImage = new Image<Gray, byte>(ImageWidth, ImageHeight);
            MinRadius = 0;
            MaxRadius = 180;
            _houghRoom = new int[ImageWidth, ImageHeight, MaxRadius - MinRadius];
        }

        public void ApplySobelFilter(int threshold)
        {
            var filter = new SobelFilter(OriginalImage, threshold, "Binary");
            SobelFilteredImage = filter.ApplyFilter();
            ApplyHoughTransformation();
        }

        private void ApplyHoughTransformation()
        {
            Array.Clear(_houghRoom, 0, _houghRoom.Length);
            var gradientDirections = new SobelFilter(OriginalImage, 0, "Directions").ApplyFilterGetAsDouble();
            var bytes = SobelFilteredImage.Bytes;
            int a, b;
            double gradient;
            for (var x = 0; x < ImageWidth; x += 1)
            {
                for (var y = 0; y < ImageHeight; y += 1)
                {
                    if (bytes[y * ImageWidth + x] > 0)  // if edge pixel
                    {
                        for (var radius = MinRadius; radius < MaxRadius; radius += 1)
                        {
                            gradient = gradientDirections[y * ImageWidth + x];  // in radians...
                            // positive direction
                            a = Rounding.ToInt(x + radius * Math.Cos(gradient));
                            b = Rounding.ToInt(y + radius * Math.Sin(gradient));
                            if (a >= 0 && a < ImageWidth && b >= 0 && b < ImageHeight)
                            {
                                _houghRoom[a, b, radius] += 1;
                            }

                            // negative direction
                            a = Rounding.ToInt(x - radius * Math.Cos(gradient));
                            b = Rounding.ToInt(y - radius * Math.Sin(gradient));
                            if (a >= 0 && a < ImageWidth && b >= 0 && b < ImageHeight)
                            {
                                _houghRoom[a, b, radius] += 1;
                            }
                        }
                    }
                }
            }
        }

        public void CalculateNormalizedHoughRoom(int radius)
        {
            var bytes = HoughRoomImage.Bytes;
            var normalized = new double[ImageWidth * ImageHeight];

            for (var x = 0; x < ImageWidth; x += 1)
            {
                for (var y = 0; y < ImageHeight; y += 1)
                {
                    normalized[y * ImageWidth + x] = _houghRoom[x, y, radius];
                }
            }

            var min = normalized.Min();
            var max = normalized.Max();
            var fraction = Convert.ToDouble(max) - min;
            for (var idx = 0; idx < (ImageWidth * ImageHeight); idx += 1)
            {
                bytes[idx] = (byte)((normalized[idx] - min) / fraction * 255.0);
            }
            HoughRoomImage.Bytes = bytes;
        }

        public void RateHoughRoom(double threshold)
        {
            var min = GetMinimum();
            var max = GetMaximum();
            var fraction = Convert.ToDouble(max) - min;
            Console.WriteLine($"min: {min}, max: {max}");

            var ratings = new List<Rating>();
            for (var radius = 0; radius < (MaxRadius - MinRadius); radius += 1)
            {
                var rating = RateHoughLayer(radius, min, fraction, threshold);
                ratings.AddRange(rating);
            }
            DrawCircles(ratings);
        }

        private List<Rating> RateHoughLayer(int radius, double min, double fraction, double threshold)
        {
            var ret = new List<Rating>();
            for (var x = 0; x < ImageWidth; x += 1)
            {
                for (var y = 0; y < ImageHeight; y += 1)
                {
                    var pixel = _houghRoom[x, y, radius];
                    if (((pixel - min) / fraction) >= threshold)
                    {
                        ret.Add(new Rating { Intensity = pixel, Radius = radius, X = x, Y = y });
                    }
                }
            }
            return ret;
        }

        private void DrawCircles(IList<Rating> ratings)
        {
            HoughImage = OriginalImage.Convert<Bgr, byte>();
            var bytes = HoughImage.Bytes; // for b,g,r channels
            int xPos, yPos, idx;
            foreach (var rating in ratings)
            {
                for (var degree = 0; degree < 360; degree += 1)
                {
                    xPos = Rounding.ToInt(rating.X + rating.Radius * Math.Cos(degree * Math.PI / 180.0));
                    yPos = Rounding.ToInt(rating.Y + rating.Radius * Math.Sin(degree * Math.PI / 180.0));
                    idx = (yPos * ImageWidth + xPos) * 3;
                    if (idx >= 0 && idx < (ImageWidth * ImageHeight * 3))
                    {
                        bytes[idx] = 0;
                        bytes[idx + 1] = 0;
                        bytes[idx + 2] = 255;
                    }
                }
            }
            HoughImage.Bytes = bytes;
        }

        private double GetMinimum()
        {
            var min = int.MaxValue;
            int val;
            for (var x = 0; x < ImageWidth; x += 1)
            {
                for (var y = 0; y < ImageHeight; y += 1)
                {
                    for (var radius = 0; radius < (MaxRadius - MinRadius); radius += 1)
                    {
                        val = _houghRoom[x, y, radius];
                        if (val < min)
                        {
                            min = val;
                        }
                    }
                }
            }
            return Convert.ToDouble(min);
        }

        private double GetMaximum()
        {
            var max = int.MinValue;
            int val;
            for (var x = 0; x < ImageWidth; x += 1)
            {
                for (var y = 0; y < ImageHeight; y += 1)
                {
                    for (var radius = 0; radius < (MaxRadius - MinRadius); radius += 1)
                    {
                        val = _houghRoom[x, y, radius];
                        if (val > max)
                        {
                            max = val;
                        }
                    }
                }
            }
            return Convert.ToDouble(max);
        }

        public class Rating
        {
            public int Intensity { get; set; }
            public int Radius { get; set; }
            public int X { get; set; }
            public int Y { get; set; }
            public override string ToString()
            {
                return $"Rating: [Intensity: {Intensity}, Radius: {Radius}, X: {X}, Y: {Y}]";
            }
        }

    }
}
