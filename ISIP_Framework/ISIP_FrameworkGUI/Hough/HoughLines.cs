﻿using Emgu.CV;
using Emgu.CV.Structure;
using ISIP_FrameworkGUI.Filter.HighPassFilter;
using ISIP_FrameworkGUI.ImageTools;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ISIP_FrameworkGUI.Hough
{
    public class HoughLines
    {
        public Image<Gray, byte> OriginalImage { get; private set; }
        public Image<Gray, byte> SobelFilteredImage { get; private set; }
        public Image<Gray, byte> HoughRoomImage { get; private set; }
        public Image<Bgr, byte> HoughImage { get; private set; }
        public int ImageWidth { get; private set; }
        public int ImageHeight { get; private set; }
        public double MaxRadius { get; private set; }
        public double MinRadius { get; private set; }
        public int WidthRadius { get; private set; }
        public int HeightAlpha { get; private set; }
        private int[] _houghRoom;

        public HoughLines(Image<Gray, byte> image)
        {
            OriginalImage = image;
            ImageWidth = OriginalImage.Width;
            ImageHeight = OriginalImage.Height;
            MaxRadius = Math.Sqrt((ImageHeight * ImageHeight + ImageWidth * ImageWidth));
            MinRadius = 0;
            WidthRadius = Rounding.ToInt(MaxRadius - MinRadius);
            HeightAlpha = 180;
            HoughRoomImage = new Image<Gray, byte>(WidthRadius, HeightAlpha);
            HoughImage = new Image<Bgr, byte>(ImageWidth, ImageHeight);
        }

        public void ApplySobelFilter(int threshold)
        {
            SobelFilteredImage = new SobelFilter(OriginalImage, threshold, "Binary").ApplyFilter();
        }

        public void CalculateHoughRoom()
        {
            _houghRoom = new int[HeightAlpha * WidthRadius];
            var bytes = SobelFilteredImage.Bytes;
            for (int y = 0; y < ImageHeight; y += 1)
            {
                for (int x = 0; x < ImageWidth; x += 1)
                {
                    if (bytes[y * ImageWidth + x] == 255)
                    {
                        for (int alpha = 0; alpha < 180; alpha += 1)
                        {
                            var radius = Rounding.ToInt((x * Math.Cos(alpha * Math.PI / 180.0) + y * Math.Sin(alpha * Math.PI / 180.0)));
                            if (radius >= 0) // exclude negative radii..?
                            {
                                _houghRoom[(alpha + 0) * WidthRadius + radius] += 1;  // increment cell in hough room
                            }
                        }
                    }
                }
            }
            HoughRoomImage.Bytes = ScaleHoughRoom();
        }

        private byte[] ScaleHoughRoom()
        {
            var scaledHoughRoom = new byte[HeightAlpha * WidthRadius];
            var max = Convert.ToDouble(_houghRoom.Max());
            var min = Convert.ToDouble(_houghRoom.Min());
            for (var x = 0; x < (HeightAlpha * WidthRadius); x += 1)
            {
                var scaled = (_houghRoom[x] - min) / (max - min);
                scaledHoughRoom[x] = (byte)(scaled * 255.0 + 0.5);
            }
            return scaledHoughRoom;
        }

        public void CalculateHoughLines(double factor)
        {
            var localMaximas = FindLocalMaxima(factor);
            HoughImage = OriginalImage.Convert<Bgr, byte>();
            var bytes = HoughImage.Bytes;
            int x, y, idx;

            foreach (var maxima in localMaximas)
            {
                var alpha = maxima.Alpha * Math.PI / 180.0;
                for (x = 0; x < ImageWidth; x++)
                {
                    y = Rounding.ToInt((maxima.Radius - (x * Math.Cos(alpha))) / Math.Sin(alpha));
                    if (y > 0 && y < ImageHeight)
                    {
                        idx = (y * ImageWidth + x) * 3;
                        bytes[idx] = 0;
                        bytes[idx + 1] = 0;
                        bytes[idx + 2] = 255;
                    }
                }

                for (y = 0; y < ImageHeight; y++)
                {
                    x = Rounding.ToInt((maxima.Radius - (y * Math.Sin(alpha))) / Math.Cos(alpha));
                    if (x > 0 && x < ImageWidth)
                    {
                        idx = (y * ImageWidth + x) * 3;
                        bytes[idx] = 0;
                        bytes[idx + 1] = 0;
                        bytes[idx + 2] = 255;
                    }
                }
            }
            HoughImage.Bytes = bytes;
        }

        private List<MaximaPoint> FindLocalMaxima(double factor)
        {
            var maximas = new List<MaximaPoint>();
            var thresholdLow = Rounding.ToInt(_houghRoom.Max() * factor);
            for (int alpha = 0; alpha < HeightAlpha; alpha += 1)
            {
                for (int radius = 0; radius < WidthRadius; radius += 1)
                {
                    if (_houghRoom[alpha * WidthRadius + radius] >= thresholdLow)
                    {
                        maximas.Add(new MaximaPoint { Alpha = alpha, Radius = radius });
                    }
                }
            }
            return maximas;
        }

        public class MaximaPoint
        {
            public int Radius { get; set; }
            public int Alpha { get; set; }
            public override string ToString()
            {
                return $"Maxima: [Radius: {Radius}, Alpha: {Alpha}]";
            }
        }

    }
}
