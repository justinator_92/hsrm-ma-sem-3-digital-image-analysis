﻿using System.Windows;
using System;

using ISIP_UserControlLibrary;
using ISIP_FrameworkGUI.Windows;
using ISIP_FrameworkGUI.ViewModels;
using ISIP_FrameworkGUI.Filter.LowPassFilter;
using ISIP_FrameworkGUI.Filter.HighPassFilter;
using ISIP_FrameworkGUI.ImageTools;
using ISIP_FrameworkGUI.Windows.Controls;
using ISIP_FrameworkGUI.ViewModels.Controls;
using ISIP_FrameworkGUI.Morphology;

namespace ISIP_FrameworkGUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ImageManipulation _imgManipulation;

        public MainWindow()
        {
            InitializeComponent();
            _imgManipulation = new ImageManipulation();
        }

        private void openGrayscaleImageMenuItem_Click(object sender, RoutedEventArgs e)
        {
            mainControl.LoadImageDialog(ImageType.Grayscale);
        }

        private void openColorImageMenuItem_Click(object sender, RoutedEventArgs e)
        {
            mainControl.LoadImageDialog(ImageType.Color);
        }

        private void saveProcessedImageMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (!mainControl.SaveProcessedImageToDisk())
            {
                MessageBox.Show("Processed image not available!", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void saveAsOriginalMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (mainControl.ProcessedGrayscaleImage != null)
            {
                mainControl.OriginalGrayscaleImage = mainControl.ProcessedGrayscaleImage;
            }
            else if (mainControl.ProcessedColorImage != null)
            {
                mainControl.OriginalColorImage = mainControl.ProcessedColorImage;
            }
        }

        private void mirrorImageMenuItem_Click(object sender, RoutedEventArgs e)
        {
            var mirrorImage = _imgManipulation.MirrorImage(mainControl.OriginalGrayscaleImage);
            mainControl.ProcessedGrayscaleImage = mirrorImage;
        }

        private void showGaussMenuItem_Click(object sender, RoutedEventArgs e)
        {
            var window = new GaussGraphWindow();
            window.DataContext = new GaussViewModel();
            window.Show();
        }

        private void showHistogrammMenuItem_Click(object sender, RoutedEventArgs e)
        {
            var window = new HistogrammWindow();
            window.DataContext = new HistogrammViewModel(mainControl.OriginalGrayscaleImage);
            window.Show();
        }

        private void binomialFilter5MenuItem_Click(object sender, RoutedEventArgs e)
        {
            var filter = new BinomialFilter(mainControl.OriginalGrayscaleImage, 5);
            mainControl.ProcessedGrayscaleImage = filter.ApplyFilter();
        }

        private void binomialFilter7MenuItem_Click(object sender, RoutedEventArgs e)
        {
            var filter = new BinomialFilter(mainControl.OriginalGrayscaleImage, 7);
            mainControl.ProcessedGrayscaleImage = filter.ApplyFilter();
        }

        private void SobelFilter_Click(object sender, RoutedEventArgs e)
        {
            var filter = new SobelFilter(mainControl.OriginalGrayscaleImage, 200, "Binary");
            var thresholdWindow = new ThresholdWindow();
            thresholdWindow.DataContext = new ThresholdControlViewModel("Sobel Filter", 255, (int d) =>
            {
                filter.Threshold = d;
                mainControl.ProcessedGrayscaleImage = filter.ApplyFilter();
            });
            thresholdWindow.Show();
        }

        private void AdaptiveSobelFilter_Click(object sender, RoutedEventArgs e)
        {
            var filter = new AdaptiveSobelFilter(mainControl.OriginalGrayscaleImage, 20);
            var thresholdWindow = new ThresholdWindow();
            thresholdWindow.DataContext = new ThresholdControlViewModel("L-AdaptSobel", 20, (int d) =>
            {
                filter.Factor = Convert.ToDouble(d) / 10.0;
                mainControl.ProcessedGrayscaleImage = filter.ApplyFilter();
            });
            thresholdWindow.Show();
        }

        private void BinarizeImage_Click(object sender, RoutedEventArgs e)
        {
            var thresholdWindow = new ThresholdWindow();
            thresholdWindow.DataContext = new ThresholdControlViewModel("Binarize Image", 255, (int d) =>
            {
                mainControl.ProcessedGrayscaleImage = _imgManipulation.BinarizeImage(mainControl.OriginalGrayscaleImage, d);
            });
            thresholdWindow.Show();
        }

        private void SkeletonItem_Click(object sender, RoutedEventArgs e)
        {
            var image = new SobelFilter(mainControl.OriginalGrayscaleImage, 100, "Binary").ApplyFilter();
            var skeleton = new Skeleton(image);
            mainControl.ProcessedGrayscaleImage = skeleton.CalculateSkeletonImage();
        }

        private void houghLines_Click(object sender, RoutedEventArgs e)
        {
            if (mainControl.OriginalGrayscaleImage == null)
            {
                return;
            }

            var window = new HoughLinesWindow();
            window.DataContext = new HoughLinesWindowViewModel(mainControl);
            window.Show();
        }

        private void houghCircleKnown_Click(object sender, RoutedEventArgs e)
        {
            if (mainControl.OriginalGrayscaleImage == null)
            {
                return;
            }

            var window = new HoughCircleKnownRadiusWindow();
            window.DataContext = new HoughCircleKnownRadiusViewModel(mainControl);
            window.Show();
        }

        private void houghCircleUnknown_Click(object sender, RoutedEventArgs e)
        {
            if (mainControl.OriginalGrayscaleImage == null)
            {
                return;
            }

            var window = new HoughCircleUnknownRadiusWindow();
            window.DataContext = new HoughCircleUnknownRadiusViewModel(mainControl);
            window.Show();
        }
    }
}
