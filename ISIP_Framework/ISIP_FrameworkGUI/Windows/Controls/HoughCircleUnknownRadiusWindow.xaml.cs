﻿using System.Windows;

namespace ISIP_FrameworkGUI.Windows.Controls
{
    /// <summary>
    /// Interaction logic for HoughCircleKnownRadiusWindow.xaml
    /// </summary>
    public partial class HoughCircleUnknownRadiusWindow : Window
    {
        public HoughCircleUnknownRadiusWindow()
        {
            InitializeComponent();
        }
    }
}
