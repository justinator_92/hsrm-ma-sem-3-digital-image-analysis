from Tkinter import *
from tkFileDialog import askopenfilename
import Image, ImageTk


class ImageAnalysis(object):

    def __init__(self):
        root = Tk()

        self.delta = 30

        # setting up a tkinter canvas with scrollbars
        frame = Frame(root, bd=2, relief=SUNKEN)
        frame.grid_rowconfigure(0, weight=1)
        frame.grid_columnconfigure(0, weight=1)
        xscroll = Scrollbar(frame, orient=HORIZONTAL)
        xscroll.grid(row=1, column=0, sticky=E + W)
        yscroll = Scrollbar(frame)
        yscroll.grid(row=0, column=1, sticky=N + S)
        self.canvas = Canvas(frame, bd=0, xscrollcommand=xscroll.set, yscrollcommand=yscroll.set)
        self.canvas.grid(row=0, column=0, sticky=N + S + E + W)
        xscroll.config(command=self.canvas.xview)
        yscroll.config(command=self.canvas.yview)
        frame.pack(fill=BOTH, expand=1)

        # adding the image
        #File = askopenfilename(parent=root, initialdir="C:/", title='Choose an image.')
        #self.image = Image.open("OlympicRings.bmp")
        self.image = Image.open("Houses.bmp")
        width, height = self.image.size
        self.aspectRatio = float(width) / height
        print width, height
        self.img = ImageTk.PhotoImage(self.image)
        self.canvas.create_image(0, 0, image=self.img, anchor="nw")
        self.canvas.config(scrollregion=self.canvas.bbox(ALL))

        # function to be called when mouse is clicked
        def printcoords(event):
            vec = self.image.getpixel((event.x, event.y))
            print vec
            destination = Image.new('L', self.image.size)
            width, height = self.image.size[0], self.image.size[1]
            for x in range(width):
                for y in range(height):
                    old = self.image.getpixel((x, y))
                    if  -self.delta < abs(old[0] - vec[0]) < self.delta and \
                        -self.delta < abs(old[1] - vec[1]) < self.delta and \
                        -self.delta < abs(old[2] - vec[2]) < self.delta:
                        destination.putpixel((x, y), 255)
                    else:
                        destination.putpixel((x, y), 0)
            destination.save("segmentation_output.bmp")
            print "done..."

        # mouseclick events
        self.canvas.bind("<Button-1>", printcoords)

        root.geometry('{}x{}'.format(width + 50, height + 50))
        root.title("Autocorrelation")
        root.mainloop()


if __name__ == "__main__":
    a = ImageAnalysis()

