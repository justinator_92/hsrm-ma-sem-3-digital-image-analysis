from PIL import Image


class Interpolation(object):

    def __init__(self, image, x_s, y_s, x_e, y_e):
        self.width, self.height = image.size
        self.image = image
        self.x_s = x_s
        self.y_s = y_s
        self.x_e = x_e
        self.y_e = y_e
        self.destination = Image.new('L', image.size)
        self.aspect = (x_e - x_s) / float(self.width)

    def interpolation(self):
        #image_data = []
        for w in range(self.width):
            for h in range(self.height):
                x_calc = self.x_s + w * self.aspect
                y_calc = self.y_s + h * self.aspect
                x = int(x_calc)
                y = int(y_calc)
                val = self.get_color_from_pixel(x, y, (x_calc - x), (y_calc - y))
                self.destination.putpixel((w,h), val)

        self.destination.save("interpolation_output.bmp")

    def get_color_from_pixel(self, x, y, x_d, y_d):
        value = (1 - x_d) * (1 - y_d) * self.image.getpixel((x, y)) \
                + x_d * (1 - y_d) * self.image.getpixel((x+1, y))   \
                + (1 - x_d) * y_d * self.image.getpixel((x, y+1))   \
                + x_d * y_d * self.image.getpixel((x+1,y+1))

        return int(value + 0.5)
