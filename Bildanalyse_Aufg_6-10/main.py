from Tkinter import *
from tkFileDialog import askopenfilename
import Image, ImageTk
from interpolation import Interpolation


class ImageAnalysis(object):

    UP = 0
    DOWN = 1

    def __init__(self):
        root = Tk()

        self.start_x = None
        self.start_y = None
        self.end_x = None
        self.end_x = None

        self.mouse = ImageAnalysis.UP
        self.last_rect = None

        # setting up a tkinter canvas with scrollbars
        frame = Frame(root, bd=2, relief=SUNKEN)
        frame.grid_rowconfigure(0, weight=1)
        frame.grid_columnconfigure(0, weight=1)
        xscroll = Scrollbar(frame, orient=HORIZONTAL)
        xscroll.grid(row=1, column=0, sticky=E + W)
        yscroll = Scrollbar(frame)
        yscroll.grid(row=0, column=1, sticky=N + S)
        self.canvas = Canvas(frame, bd=0, xscrollcommand=xscroll.set, yscrollcommand=yscroll.set)
        self.canvas.grid(row=0, column=0, sticky=N + S + E + W)
        xscroll.config(command=self.canvas.xview)
        yscroll.config(command=self.canvas.yview)
        frame.pack(fill=BOTH, expand=1)

        # adding the image
        File = askopenfilename(parent=root, initialdir="C:/", title='Choose an image.')
        self.image = Image.open(File)
        width, height = self.image.size
        self.aspectRatio = float(width) / height
        print width, height
        self.img = ImageTk.PhotoImage(self.image)
        self.canvas.create_image(0, 0, image=self.img, anchor="nw")
        self.canvas.config(scrollregion=self.canvas.bbox(ALL))

        # function to be called when mouse is clicked
        def printcoords(event):
            if self.mouse == ImageAnalysis.UP:
                self.start_x = event.x
                self.start_y = event.y
                self.mouse = ImageAnalysis.DOWN
                return

            deltaX = event.x - self.start_x
            deltaY = event.y - self.start_y

            if deltaX < 0 or deltaY < 0:
                return

            self.canvas.delete(self.last_rect)
            self.last_rect = self.canvas.create_rectangle(self.start_x, self.start_y,
                                                          self.start_x + deltaY * self.aspectRatio, event.y)

        def release(event):
            x, y = event.x, event.y
            inter = Interpolation(self.image, self.start_x, self.start_y, x, y)
            inter.interpolation()
            self.mouse = ImageAnalysis.UP
            print "image rendered..."

        # mouseclick events
        self.canvas.bind("<B1-Motion>", printcoords)
        self.canvas.bind("<ButtonRelease-1>", release)

        root.geometry('{}x{}'.format(width + 50, height + 50))
        root.title("Bilinear Interpolation")
        root.mainloop()

if __name__ == "__main__":
    a = ImageAnalysis()

