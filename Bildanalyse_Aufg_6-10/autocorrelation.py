from Tkinter import *
from tkFileDialog import askopenfilename
import Image, ImageTk
import matplotlib.pyplot as plt
import tkMessageBox

class ImageAnalysis(object):

    FIRST = 0
    SECOND = 1
    THIRD = 2

    def __init__(self):
        root = Tk()

        self.mouse = ImageAnalysis.FIRST
        self.first_circ = None
        self.second_circ = None
        self.third_circ = None
        self.line = -1
        self.n = float('inf')
        self.N = float('inf')
        self.k = float('inf')

        # setting up a tkinter canvas with scrollbars
        frame = Frame(root, bd=2, relief=SUNKEN)
        frame.grid_rowconfigure(0, weight=1)
        frame.grid_columnconfigure(0, weight=1)
        xscroll = Scrollbar(frame, orient=HORIZONTAL)
        xscroll.grid(row=1, column=0, sticky=E + W)
        yscroll = Scrollbar(frame)
        yscroll.grid(row=0, column=1, sticky=N + S)
        self.canvas = Canvas(frame, bd=0, xscrollcommand=xscroll.set, yscrollcommand=yscroll.set)
        self.canvas.grid(row=0, column=0, sticky=N + S + E + W)
        xscroll.config(command=self.canvas.xview)
        yscroll.config(command=self.canvas.yview)
        frame.pack(fill=BOTH, expand=1)

        # adding the image
        #File = askopenfilename(parent=root, initialdir="C:/", title='Choose an image.')
        self.image = Image.open("texture8_1.bmp")
        width, height = self.image.size
        self.aspectRatio = float(width) / height
        print width, height
        self.img = ImageTk.PhotoImage(self.image)
        self.canvas.create_image(0, 0, image=self.img, anchor="nw")
        self.canvas.config(scrollregion=self.canvas.bbox(ALL))

        # function to be called when mouse is clicked
        def printcoords(event):
            r = 4

            if self.mouse == ImageAnalysis.FIRST:
                self.line = event.y
                self.n = event.x
                print "n: " + str(self.n)
                self.mouse = ImageAnalysis.SECOND
                self.first_circ = self.canvas.create_oval(event.x - r, event.y - r, event.x + r, event.y + r)
                return
            if self.mouse == ImageAnalysis.SECOND:
                self.N = event.x
                print "N: " + str(self.N)
                self.mouse = ImageAnalysis.THIRD
                self.second_circ = self.canvas.create_oval(event.x - r, event.y - r, event.x + r, event.y + r)
                return
            if self.mouse == ImageAnalysis.THIRD:
                self.k = event.x - self.n
                print "K: " + str(self.k)
                self.third_circ = self.canvas.create_oval(event.x - r, event.y - r, event.x + r, event.y + r)

                corr_values = self.calculate_auto_correlation()

                self.canvas.delete(self.first_circ)
                self.canvas.delete(self.second_circ)
                self.canvas.delete(self.third_circ)

                x_coords = [x for x in range(int(self.n), int(self.N))]
                y_coords = [self.image.getpixel((x, self.line)) for x in range(len(x_coords))]

                plt.figure(1)
                plt.title("Gray level profile")
                plt.plot(x_coords, y_coords, '')
                plt.axis([self.n, self.N, 0, 255])

                plt.figure(2)
                plt.title("autocorrelation")
                plt.plot([x for x in range(0, len(corr_values))], corr_values, '')
                plt.axis([0, len(corr_values), 0.95, 1])

                print "K: " + str(self.k) + " max: " + str(max(corr_values)) + " k: " + str(corr_values.index(max(corr_values)))
                plt.show()

                self.mouse = ImageAnalysis.FIRST

        # mouseclick events
        self.canvas.bind("<Button-1>", printcoords)

        root.geometry('{}x{}'.format(width + 50, height + 50))
        root.title("Autocorrelation")
        root.mainloop()

    def calculate_auto_correlation(self):
        lis = []
        r_0 = 0

        for n in range(self.n, self.n + self.k * 2):
            g_x = self.image.getpixel((n, self.line))
            r_0 += (g_x * g_x)

        print "r0: " + str(r_0)

        for counter in range(0, self.k):
            r_k = self.R_k(counter)
            lis.append(r_k / float(r_0))

        return lis

    def R_k(self, k):
        r_k = 0
        for n in range(self.n, self.n + self.k * 2):
            g_x = self.image.getpixel((n, self.line))
            g_x_k = self.image.getpixel((n + k, self.line))
            r_k += (g_x * g_x_k)
        return r_k



if __name__ == "__main__":
    a = ImageAnalysis()

